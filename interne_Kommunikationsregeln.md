# PLQ-interne Kommunikationsregeln

## Allgemeines

Für einen konstruktiven Austausch innerhalb der Partei gilt es bestimmte Regeln zu beachten, die dafür sorgen sollen, dass möglichst viele Stimmen gehört und darauf aufbauend "gute" Entscheidungen getroffen werden können.
Für die digitale Kommunikation haben wir folgende Diskussionszeichen festgelegt, die im Protokoll-Chat angewendet werden und am unteren Protokollende während des gesamten Treffens sichtbar sein sollen:

    Wortmeldung: #     "Direkt-dazu": ##     Prozessvorschlag: P      Verständnisfrage: ?
    Zustimmung: +      Ablehnung: -      "Klärt das zu zweit": 1:1

## Moderation

In kleineren Runden (bis ca. 5 Personen) ist es möglich, ohne Moderation gemeinsam auf die Reihenfolge & Verteilung der Redeanteile zu achten. Dabei sind auf vorhergehende Beiträge reagierende Beiträge (##) vorzuziehen sowie entstehende Fragen (?) vor neuen Beiträgen zu klären. Prozessvorschläge können dazu dienen, die Diskussion anzukurbeln oder ggf. Wiederholungen zu vermeiden. Um die gerade in der digitalen Kommunikation so berüchtigten Pausen nach Redebeiträgen zu überbrücken, sollen Redebeiträge möglichst mit "Ende" beendet werden. Durch dieses Signal weiß die nächste auf der Redeliste stehende Person bzw. die Moderation, dass sie an der Reihe ist.

Generell ist eine Moderation für die Verteilung gleicher Redeanteile sinnvoll und mit steigender Personenzahl unbedingt notwendig. Diese kann sich zu zweit geteilt werden, wobei die "stellvertretende" Moderation als Unterstützung agiert und einspringt, wenn bspw. die "Hauptmoderation" einen eigenen Redebeitrag hervorbingen möchte. Auch sollte die Stellvertretung darauf achten, dass die Hauptmoderation eine gewisse Neutralität beibehält und ggf. eingreifen. Dies kann bspw. durch einen kurzen Hinweis wie "Das ist jetzt deine Position, ich übernehme die Moderation" geschehen. Die Hauptmoderation kann auch darum bitten, kurz vertreten zu werden, um ihre eigene Position zu äußern.

Eine Herausforderung ist das sogenannte [Gruppendenken](https://de.wikipedia.org/wiki/Gruppendenken), das sich ergibt, wenn jede Person ihre Meinung an die erwartete Gruppenmeinung anpasst. Dies kann die Moderation bzw. die Gruppe durch die aktive Aufforderung zur Äußerung von Gegenargumenten unterbinden. Ein anonymes Feedback durch das entwickelte [Entscheidungstool](moodpoll.uber.space/) beugt ebenfalls schlechten Entscheidungen vor. Wichtig ist zudem eine Gesprächsatmosphäre, in der Gegenargumente und Widerspruch nicht als persönliche Kritik aufgefasst werden, sondern als willkommene Prüfung für die vorherrschende oder zuvor geäußerte Ansicht. Echte Bedenken, entgegengestellte Gedankenspiele und ein Einlenken oder Ändern des persönlichen Standpunkts werden unabhängig von der politischen Vorerfahrung, fachlichen Expertise oder Dauer der Mitgliedschaft dankbar aufgenommen und in angemessener Weise berücksichtigt. Die Äußerung dieser bedarf größerer Courage als simple Zustimmung.

In konfliktiven Situationen sind alle dazu aufgefordert, sich in die Diskussion einzuschalten und durch einen Prozessvorschlag die Auseinandersetzung zu unterbrechen.

Für eine klare, wertschätzende und konstruktive Kommunikation sind folgende Richtlinien hilfreich:

- möglichst kurz fassen (1-2 Minuten pro Redebeitrag)
- Ich-Botschaften (statt "man", "wir") & respektvolles Miteinander (sach- anstatt personenorientierte Gesprächsebene)
- Ausreden lassen -> Beitrag mit "Ende" beenden
- Vorsicht mit Verallgemeinerungen (z.B. immer, nie, alle, ...)
- *Konstruktive* Kritik und Wertschätzung ausdrücken
- gendergerechte Sprache (auf gewünschte Pronomen achten, männliche und weibliche Formen nennen etc.)
- eventuelle versehentliche Verstöße gegen (genderbezogene) Sprachnormen, insbesondere mündlich "im Eifer des Gefechts", sollten nicht als üble Absicht oder Angriff aufgefasst werden - eventuell einen freundlichen Hinweis geben
- Allgemein: Selbst wenn jemand etwas sagt, das einem gar nicht passt: Grundannahme sollte sein, dass Andere einem selbst und der gemeinsamen Sache der PLQ wohlgesonnen sind. Vielleicht sind scheinbare "Angriffe" eher ein Missverständnis, das in Ruhe geklärt werden kann?


## Verfahren/Methoden

Für eine produktive Diskussion sind Zusammenfassungen zwischendurch gut, um das bereits Gesagte zu bündeln. Falls Diskussionen zum Stocken kommen oder sich im Kreis drehen, sind Stimmungsbilder für das weitere Vorgehen hilfreich. Dazu kann ein Vorschlag aus den vorgebrachten Redebeiträgen zusammengefasst werden, der wiederum zu einer vorläufigen Abstimmung gestellt wird. An dieser Stelle sind alle aufgefordert, ihre Position dazu kundzutun. Entweder, indem jede*r Einzelne reihum etwas sagt bzw. eventuelle Bedenken äußert, oder durch eine per Hand-/Diskussionszeichen ausgeführte Abstimmung (z.B. zählt die Anzahl der Finger für die Stärke der Zustimmung bzw. schriftlich per +++ bis --) Darauf basierend kann die Diskussion eine neue Ausrichtung erfahren und ggf. neue Vorschläge entstehen lassen, die schließlich über Systemisches Konsensieren (und das entsprechende Tool) zu einer Entscheidung führen.
