# Gemeinsame Entscheidungsfindung in Gruppen

*Durch Konsensbeschluss in Kraft gesetzt am 21.09.2020.*

## Allgemeines

Die Art, wie Entscheidungen getroffen werden, hat maßgeblichen Einfluss darauf, welche Entscheidungen getroffen werden. Oft stehen mehrere Varianten („Optionen”) zur Auswahl. Wir streben daher einen partizipativen Entscheidungsfindungsprozess an, in den alle relevanten Argumente einfließen und dessen Ergebnis in der Gruppe möglichst wenig Unzufriedenheit bzw. Widerstand und möglichst viel Zufriedenheit auslöst. Eine Entscheidungsfindung besteht aus einem Diskussionsprozess und einer Abstimmung, für die jeweils eigene Fristen und Regelungen gelten. Für die Abstimmung orientieren wir uns an der Methode des systemischen Konsensierens und setzen, falls notwendig, zur Vereinfachung technische Hilfsmittel ein.

Es gibt folgende Abstimmungskategorien:

- „Wichtig”: Abstimmungszeit 14 volle Tage, Mindestquorum für Gültigkeit zwei Drittel der Stimmberechtigten

- „Normal“: Abstimmungszeit 7 volle Tage, kein Mindestquorum
- „Echtzeit”: Abstimmungszeit 5 min, Mindestquorum 100% der jeweiligen Gruppe

(Mehrtägige Fristen enden immer Mitternacht, Zeitzone Berlin.)

Für jede Abstimmung wird eine verantwortliche Person (“Hutperson”) und eine Stellvertretung bestimmt. Die Hutperson kümmert sich um die organisatorische und technische Realisierung und Archivierung der Abstimmung, die Stellvertretung unterstützt und kontrolliert die Einhaltung der hier dokumentierten Regeln. Alle laufenden (außer Kategorie „Echtzeit”) und alle abgeschlossenen Abstimmungen sind auf dem Datei-Server im Verzeichnis *Formelles/Abstimmungen* abzulegen.

Zur Manipulationssicherheit und Nachvollziehbarkeit sind alle Aktivitäten sowohl im Diskussionsprozess als auch während der Abstimmung mit Nennung des Klarnamens durchzuführen. Anonyme Beiträge sind ungültig.

## Diskussionsprozess und Vorbereitung

Eine Abstimmungsinitiative kann von jedem Gruppenmitglied individuell

(„Initiator:in“) oder kollektiv von den Teilnehmenden an einer

Gruppendiskussion gestartet werden, sofern diese:r nicht zeitweise vom Initiativrecht gesperrt ist/sind (siehe unten, Punkt 11).

Zur Vorbereitung der Abstimmung müssen folgende Aspekte festgelegt werden:

1. ​	der Titel der Abstimmung
2. ​	die zur Abstimmung stehende(n) Option(en) (konkrete Formulierung, Reihenfolge),
3. ​	die Abstimmungskategorie (siehe oben),
4. ​	die Gruppe (und damit Anzahl) der Stimmberechtigten sowie
5. ​	Hutperson und Stellvertretung.

Der:die Initiator:in macht Vorschläge für die Punkte 1 bis 4. Abstimmungskategorie und Gruppe der Stimmberechtigten sind zu begründen. Die Gruppe muss innerhalb der Partei strukturell definiert sein und entsprechend *Dokument Parteigliederung* zuständig sein. Die Hutperson sollte in der Gruppe rotierend festgelegt werden. Eine:r der Initiator:innen kann die Stellvertretung übernehmen. Bedenken zu den Punkten können in der Diskussion vorgebracht werden. Alle Gruppenmitglieder können während des Diskussionsprozesses Abstimmungsoptionen hinzufügen. Das Ändern und Entfernen von Abstimmungsoptionen ist nur mit Einverständnis der:des Urheber:in der Option zulässig.

Grundsätzlich findet der Diskussionprozess asynchron (d.h. über mehrere Tage hinweg) statt und dauert je nach festgelegter Abstimmungskategorie 7 Tage (Kategorie „Normal“) bzw. 14 Tage (Kategorie „Wichtig“). Der Diskussionsprozess kann durch Konsensbeschluss mit mindestens 50% der stimmberechtigten Mitglieder der Gruppe beliebig verlängert, verkürzt oder sofort beendet werden.

Der asynchrone Vorbereitungsprozess (sofern nicht durch Konsensbeschluss übersprungen) wird über das Verzeichnis *Formelles/Abstimmungen/in_Vorbereitung* organisiert und startet mit einer Bekanntgabe durch die Hutperson über das primäre Kommunikationsmedium (z.B. Mailingliste) der Gruppe.

Die eigentliche Abstimmung startet durch eine entsprechende Nachricht der Hutperson über das primäre Kommunikationsmedium.

Kategorienbestimmung und -bedeutung

Die Kategorien unterscheiden sich nach erforderlichem Quorum und Abstimmungsdauer, siehe oben.

In die Kategorie “Wichtig” fällt alles, was das Wesen der Partei ausmacht, insbesondere Entscheidungen zu den Grundsatzregeln (Grundwerte, Verhaltens- und Kommunikationsregeln, Satzung, Regeln zur Entscheidungsfindung) und zum Grundsatzprogramm. Weitere Entscheidungen können je nach Ermessen einer der oben genannten Kategorien zugeordnet werden.

Eine Entscheidung der Kategorie “Wichtig” kann zukünftig auch nur durch eine Entscheidung dieser Kategorie geändert oder aufgehoben werden.

## Abstimmungs- und Auswerteregeln

1. Jede stimmberechtigte Person vergibt für jede Option einen ganzzahligen Wert zwischen -3 (maximaler Widerstand) und +2 (maximale Zustimmung).  

2. Der Wert -3 wird als Veto aufgefasst, der Wert 0 als neutrale Haltung.

3. Jede Abstimmungsvorlage enthält obligatorisch zwei abschließende Prüffragen (Zufriedenheitsangabe, ZA), nämlich: a)  “Ich bin formal mit der Gestaltung dieser Abstimmungsvorlage zufrieden.” (ZAG) , sowie b) “Diese Initiative ist inhaltlich sinnvoll für die Arbeit der Gruppe.” (ZAS). Bei beiden Optionen wird bei der Auswertung nur zwischen Veto und Nicht-Veto unterschieden. 

   Die ZAG stellt sicher, dass Optionsvorschläge oder inhaltliche Bedenken von Stimmberechtigten, die bisher nicht im Diskussionsprozess beteiligt waren, noch Eingang finden können. Die ZAS dient dazu, grob unsinnige und kontraproduktive Initiativen zu verhindern. 
   

4. Die Auswertung beginnt nach Ende der Frist oder wenn 100% der Stimmberechtigten abgestimmt haben.

5. Zur Auswertung werden für jede Option negative und positive Punkte getrennt aufsummiert. Für jede Option ergeben sich  folgende Werte: Anzahl der Vetos,  Gesamtwiderstand und Gesamtzustimmung.


   **Wenn es keine Vetos gibt:**

6. Es gewinnt die Option mit dem geringsten Gesamtwiderstand. Bei Gleichheit mehrerer Optionen entscheidet die Gesamtzustimmung. Bei weiterer Gleichheit werden die Widerstands-/Zustimmungsstufen einzeln betrachtet: Es werden zunächst alle Optionen gestrichen, die die meisten “-3”-Werte bekommen haben; wenn alle Optionen gleichviele “-3”-Werte haben, wird keine gestrichen. Wenn nur eine Option verbleibt, gewinnt diese; andernfalls  werden in der gleichen Weise die Optionen mit den meisten “-2”-Werten gestrichen, bei weiterer Gleichheit die Optionen mit den meisten “-1”-Werten etc. Falls am Ende immer noch mehrere Optionen verbleiben, entscheidet der Zufall (der Optionstext mit der kleinsten Prüfsumme (“Hash-Wert”) des sha256-Algorithmus).


   **Wenn es mindestens ein Veto gibt:**

7. Ein Veto ist nur gültig, wenn zum Abstimmungsende eine Begründung dafür vorliegt (mindestens ein vollständiger Satz, der sich inhaltlich auf die Option bzw. die Abstimmung bezieht; bei Kategorie “Echtzeit”: mündlich, bei Kategorie “Wichtig” und “Normal”: im Verzeichnis der Abstimmung). Vetos ohne Begründung werden ignoriert. Der Widerstandswert -3 wird aber berücksichtigt (bei inhaltlichen Vetos).

8. Die Begründung für ein ZAG-Veto muss mindestens einen Veränderungsvorschlag für die Abstimmung beinhalten (bezogen auf Titel, Option(en), Abstimmungskategorie, Gruppe der Stimmberechtigten oder Hutperson/Stellvertretung), sonst ist das Veto ungültig.

9. Ein Veto für eine inhaltliche Option (nicht: ZA) ist nur relevant, wenn diese Option nach Regel 5 gewinnen würde. Das bedeutet, dass die Gruppe insgesamt mit dieser Option am besten leben könnte, aber mindestens eine Person überhaupt nicht.

10. Sobald ein gültiges und relevantes inhaltliches oder ZAG- Veto vorliegt, wird der offizielle Diskussionsprozess nach obigen Bestimmungen und Fristen wiederaufgenommen und von der Hutperson organisiert. Wenn diese ergänzende Diskussion beendet ist, wird die Abstimmung neu gestartet. Im Beschreibungstext wird auf die Veto-Begründungen aus der ersten Runde hingewiesen.

11. Wenn mindestens 50% der Stimmberechtigen ein ZAS-Veto aussprechen, wird der Entscheidungsfindungsprozess abgebrochen, und dem/der/den Initiator:in(en) wird für drei Monate das Initiativrecht aberkannt. 


    **Zweiter oder späterer Durchlauf nach einem Veto-Prozess**

12. Beträgt die Anzahl gültiger ZAG-Vetos weniger als 10% der Gesamtstimmberechtigten, werden diese übergangen. Andernfalls wird der offizielle Diskussionsprozess nach obigen Bestimmungen und Fristen wieder aufgenommen und von der Hutperson organisiert. Dabei wird die Option “Dieser Entscheidungsprozess sollte abgebrochen werden.” hinzugefügt.

13. Von den inhaltlichen Optionen werden all jene gestrichen, deren Veto-Anzahl mindestens 10% der Gesamtstimmberechtigten ausmacht. Unter den verbleibenden Optionen wird die gewinnende Option nach Regel 5 bestimmt. Wenn keine Option verbleibt, wird der offizielle Diskussionsprozess nach obigen Bestimmungen und Fristen wieder aufgenommen und von der Hutperson organisiert.    

## Abschluss der Entscheidungsfindung

Wenn eine endgültige Entscheidung gefunden wurde, wird diese innerhalb von 24h durch die Hutperson über das primäre Kommunikationsmedium bekanntgegeben und archiviert. 


  