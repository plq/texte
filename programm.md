
## Vorbemerkung

Dies ist die erste Version unseres Parteiprogramms. Es soll kontinuierlich weiterentwickelt und kritisch hinterfragt werden. Wir bitten deshalb aktiv um **Feedback**. Welche Probleme haben wir übersehen und welche überbewertet? Welche Vorschläge sind zu zaghaft, welche zu weitgehend? Wir freuen uns über Rückmeldungen und ehrliche Meinungen. Als Gründungsinitiative einer neuen Partei bieten wir aktiven Gestaltungsspielraum. Lasst uns diesen zum Wohle der Menschen nutzen!





## Unser Leitbild


Jeder Mensch hat das Recht auf ein menschenwürdiges Leben. Die Basis dafür ist die **Erfüllung von Grundbedürfnissen** wie Ernährung, Gesundheit, Wohnen, Sicherheit und soziale Teilhabe. Doch diese vermeintlichen Selbstverständlichkeiten sind nicht sicher: Zum einen bedroht das aktuelle Wirtschaftssystem die Lebensgrundlagen von Menschen in weiten Teilen der Welt sowie zukünftiger Generationen, zum anderen herrscht schon jetzt für viele Menschen Perspektivlosigkeit und Existenznot. Die weltweiten Entscheidungsstrukturen sind derzeit auf die Interessen der Wirtschaft und des Kapitalmarkts ausgerichtet. Unter dem &quot;Primat der Ökonomie&quot; bestimmen Staaten und transnationale Konzerne die Regeln zu ihrem ökonomischen Vorteil und dabei meist zu Lasten der Umwelt und der sozialen Gerechtigkeit. Das kapitalistische System zerstört damit seine eigenen Existenzgrundlagen. Politik muss dafür sorgen, dass die Welt ein lebenswerter Ort bleibt und die verfügbaren Ressourcen gerechter verteilt werden. Eine einseitig an wirtschaftlichem Wachstum orientierte Politik wird an beiden Zielen scheitern.




<hr class="hr_gradient">
<div class="program_quote">
Politik muss dafür sorgen, dass die Welt
ein lebenswerter Ort bleibt und die verfügbaren Ressourcen gerechter verteilt werden.
</div>
<hr class="hr_gradient">

Die PLQ tritt für eine Politik der Lebensqualität ein und setzt somit den Menschen in den Mittelpunkt. Wir möchten, dass Menschen weltweit – jetzt und in Zukunft – ein Leben in Würde führen können. Dazu gehört in erster Linie die Befriedigung von Grundbedürfnissen wie Ernährung, Gesundheit, Wohnen, Sicherheit und soziale Teilhabe. **Lebensqualität** ist aber mehr als die Befriedigung von Grundbedürfnissen. Lebensqualität bedeutet, ein gutes Leben zu führen. Dafür brauchen wir Zukunftsperspektiven, die uns optimistisch stimmen und uns Lebensfreude bringen. Wir brauchen Zeit und Ruhe und Privatsphäre – auch um uns zu überlegen, welches Leben wir führen wollen.


Um diese Ziele zu erreichen, bedarf es einer Gesellschaft, die nicht von permanentem Wachstum abhängig ist. Die Politik muss sich in erster Linie auf Mensch und Natur konzentrieren, nicht auf Wirtschaftsleistung und Arbeitsplätze. Wir treten für eine Gesellschaft ein, die das Wachstums- und Leistungsdogma konsequent überwindet und fordern daher eine sozial und ökologisch regulierte Marktwirtschaft. Lebensqualität muss zum zentralen Leitmotiv der Politik werden.

## Die zentralen Probleme unserer Zeit
### 1. Ökologische Nicht-Nachhaltigkeit in Deutschland und Europa


Um den Klimawandel und den Raubbau an der Natur zu stoppen, fordern wir eine schnellstmögliche **Umstellung auf erneuerbare Energien**. Alle Kohle- und Atomkraftwerke auf dem Gebiet der EU müssen innerhalb weniger Jahre vom Netz. Stattdessen müssen Wind-, Sonnen- und Bioenergie stärker gefördert werden. Da nur noch wenige Flüsse in Deutschland unverbaut sind und sich vor allem kleine Wasserkraftwerke als ineffizient erwiesen haben, setzen wir uns innerhalb Deutschlands für die Renaturierung verbauter Flüsse und den Rückbau kleiner Wasserkraftwerke ein. Stattdessen sollen große Wasserkraftwerke modernisiert werden, um sie auf den aktuellen technischen und ökologischen Stand zu bringen. Da die flächendeckende Nutzung erneuerbarer Energien zurzeit durch fehlende Speichermöglichkeiten erschwert wird, setzen wir uns für ein Energiespeichergesetz ein, das die Entwicklung und Bereitstellung von Energiespeichermedien unabhängig von der Technologie finanziell fördert. Um dezentrale Energiekonzepte zu entwickeln, sollen Kommunen und Landkreise dabei unterstützt werden, einen lokalen und nachhaltigen Energiemix zu produzieren, der sie weitestgehend unabhängig von den großen Verbundnetzen macht.


<hr class="hr_gradient">
<div class="program_quote">
Politik muss sich in erster Linie
auf Mensch und Natur konzentrieren,
nicht auf Wirtschaftsleistung und Arbeitsplätze.
</div>
<hr class="hr_gradient">

Technologische Innovationen zur umweltschonenden Stromerzeugung können hilfreich sein, dürfen uns aber nicht davon ablenken, dass für eine zukunftsfähige Entwicklung eine generelle **Senkung des Energieverbrauchs** vonnöten ist. Der weltweite Energieverbrauch steigt seit Jahrzehnten kontinuierlich an. Vor allem in den Industrie- und Erdölländern ist der Pro-Kopf-Energieverbrauch erschreckend hoch und muss dringend gesenkt werden. Dafür schlagen wir – sowohl innerhalb Deutschlands als auch innerhalb der EU – eine spürbare Erhöhung der Energie- und Stromsteuer vor. Neben Strom, Benzin, Diesel, Erdgas, Flüssiggas, Schweröl und Kohle müssen in Zukunft auch Kerosin und Flugbenzin besteuert werden. Damit soll ein deutlicher Anreiz zum Energiesparen geschaffen werden.




Die Einnahmen dieser Steuern sollen dafür genutzt werden, den Produktionsfaktor Arbeit zu entlasten. Indem **Energie teurer** wird und **Arbeit günstiger**, z. B. durch Senkung der Lohnnebenkosten, können wir nicht nur den Energieverbrauch in der industriellen Produktion senken, sondern auch dem absehbaren Arbeitsplatzverlust durch Automatisierung entgegenwirken. Im Zweifelsfall lohnt es sich dann eher, eine weitere Arbeitskraft einzustellen als eine neue Maschine anzuschaffen, die energieaufwändig produziert und betrieben werden muss. Auch weite Transportstrecken in der Herstellung und Vermarktung von Produkten würden sich ökonomisch nicht mehr rentieren. Für die Verbraucher\*innen würde sich die Reparatur von defekten Geräten endlich wieder lohnen, weil sie deutlich günstiger wäre als ein Geräteneukauf. Da der Preis vieler Produkte durch die Erhöhung der Energie- und Stromsteuer steigen würde, sollen die Einnahmen außerdem dazu genutzt werden, die unteren Einkommensgruppen finanziell zu entlasten, z. B. durch Senkung der Einkommenssteuer. Langfristig sollen die **Preise von Produkten** sowohl die ökologischen als auch die sozialen Folgekosten ihrer Produktion widerspiegeln. Das heißt: Produkte, die unter ökologisch und sozial sinnvollen Bedingungen hergestellt wurden, sollen günstiger sein als nicht-nachhaltige Produkte. Um dies zu gewährleisten, sollen Produkte mit einer guten Gemeinwohlbilanz steuerlich entlastet werden (Konzept der Gemeinwohl-Ökonomie).


<hr class="hr_gradient"><div class="program_quote">
Produkte, die unter ökologisch und
sozial sinnvollen Bedingungen
hergestellt wurden, müssen
günstiger sein als nicht-nachhaltige Produkte.
</div><hr class="hr_gradient">



Um die Lebensmittelproduktion in Deutschland und Europa nachhaltiger zu gestalten, fordern wir einen sofortigen Stopp der Agrarsubventionen für Massentierhaltung und konventionelle Landwirtschaft. Öffentliche Fördermittel sollen in Zukunft nur an Betriebe vergeben werden, die nachweisbar ökologisch wirtschaften. Vor allem die Einhaltung der EU-Grenzwerte für Ammoniak und Nitrat muss in Zukunft stärker kontrolliert werden. Langfristig streben wir ein Verbot der Massentierhaltung innerhalb der EU sowie eine komplette **Umstellung auf ökologische Landwirtschaftsmodelle** an. Die innerhalb der EU erhältlichen Produkte müssen dann mindestens den Kriterien des heutigen EU-Bio-Siegels entsprechen. Insgesamt brauchen wir eine höhere Wertschätzung für unsere Lebensmittel. Außerdem setzen wir uns für eine massive Reduktion des Einsatzes von Agrar-Chemie ein, durch welche Probleme wie Insektensterben, multiresistente Keime und Grundwasserverschmutzung verursacht werden.


Im Bereich Mobilität setzen wir uns für die **Förderung des Fahrrads und des öffentlichen Verkehrs** ein. Innenstädte müssen weitestgehend autofrei gestaltet werden. Weniger Autos bedeuten auch weniger Staus, bessere Luft, weniger Lärm und weniger Stress. Um dieses Ziel zu erreichen, braucht es breite, baulich vom Autoverkehr getrennte Fahrradwege und einen attraktiveren Öffentlichen Personennahverkehr (ÖPNV). Busse und Bahnen im Innenstadtbereich müssen in vielen Städten deutlich häufiger fahren und intelligenter geplant werden. Auch der ländliche Raum würde von einem besser ausgebauten ÖPNV-System profitieren. Insgesamt muss der ÖPNV für alle Menschen finanziell erschwinglich sein. Um das Pendeln zwischen zwei nahegelegenen Städten zu erleichtern, sollen Fahrradschnellwege weiter ausgebaut werden. Auch im Fernverkehr müssen Busse und Bahnen attraktiver werden. Eine Fahrt mit Zug oder Fernbus sollte in jedem Fall günstiger sein als ein Kurzstreckenflug. Dafür müssen auch die bisherigen Subventionen fürs Fliegen abgeschafft werden. Um den Kraftstoffverbrauch im Individualverkehr zu senken sowie die Sicherheit auf den Straßen zu erhöhen, setzen wir uns zudem für ein Tempolimit von 130 km/h auf allen deutschen Autobahnen ein.

### 2. Gefährdete Demokratie in Deutschland und Europa


Unter Demokratie verstehen wir die Umsetzung des Mehrheitswillens unter Berücksichtigung von Minderheitenrechten. Beispiele aus der älteren und jüngeren Geschichte zeigen, dass diese gesellschaftliche Errungenschaft verschiedenen Gefahren ausgesetzt ist. Einerseits werden zu einfache Lösungen mit Absolutheitsanspruch mehrheitsfähig, andererseits ermöglichen moderne IT-Systeme die Überwachung und Kontrolle sowohl einzelner Personen als auch großer Bevölkerungsteile. Ein weiteres großes Problem ist die immer weiter zunehmende Konzentration von wirtschaftlicher Macht bei einigen wenigen Konzernen wie z. B. Banken, Investmentfonds und globale Aktiengesellschaften, und ihr Einfluss auf politische Prozesse ohne jede demokratische Legitimation. Dies geschieht z. B. durch Parteienfinanzierung, Einfluss auf Medien (Werbekunden) oder auch Korruption. Um die freiheitlich-demokratische Grundordnung gegenüber diesen Herausforderungen zu stärken, fordern wir u. a. eine Reform des Bildungssystems, der Sicherheitsbehörden, des Umgangs mit IT-Systemen, eine Wahlrechtsreform, eine deutlich verbesserte demokratische Legitimation der EU und ein strikteres Kartellrecht.



<hr class="hr_gradient"><div class="program_quote">
Ein großes Problem ist die immer
weiter zunehmende Konzentration
von wirtschaftlicher Macht bei
einigen wenigen Konzernen.
</div><hr class="hr_gradient">



Unzweifelhaft wird die Welt immer komplexer und schwieriger zu verstehen. Deshalb sollte das **Bildungssystem** nicht primär der pragmatischen Berufsvorbereitung dienen, sondern Menschen die Fähigkeit zum selbstständigen und kritischen Denken vermitteln. Insbesondere im Umgang mit klassischen und sozialen Medien besteht aus unserer Sicht Nachholbedarf. Dadurch wird langfristig die Gefahr reduziert, dass einfache Scheinlösungen grundlegende demokratische Errungenschaften existenziell bedrohen. Zudem sind Kompromissbereitschaft und Empathie unabdingbare Voraussetzungen für das Funktionieren einer demokratischen Gesellschaft und müssen dementsprechend im Bildungssystem gefördert werden.

<hr class="hr_gradient"><div class="program_quote">
Das Bildungssystem sollte nicht primär
der pragmatischen Berufsvorbereitung dienen,
sondern Menschen die Fähigkeit zum selbstständigen
und kritischen Denken vermitteln.
</div><hr class="hr_gradient">


Des Weiteren gefährden auch **unregulierte Datensammlungen** und darauf aufbauende Algorithmen die Demokratie, z. B. indem sie die Mediennutzung und damit die demokratische Meinungs- und Willensbildung beeinflussen (Stichwort: Filterblasen). Deshalb fordern wir Transparenz bei der Datennutzung und -weitergabe sowie das Recht auf unkomplizierte Auskunft und Löschung von personenbezogenen Daten. Um **Transparenz in den IT-Systemen** insgesamt zu fördern, sollte möglichst viel freie und damit quelloffene Software verwendet werden, dies gilt insbesondere für öffentliche IT-Investitionen (Stichwort: Public Money – Public Code). Die ethische Frage, welche Entscheidungen durch Algorithmen überhaupt getroffen werden dürfen, wird in absehbarer Zeit hochrelevant. Darf beispielsweise ein Konzern auf Basis von Persönlichkeitsmodellen (Wahl-)Werbung auf einzelne Wähler\*innen individuell zuschneiden? Um solche Fragen zu beantworten, setzen wir uns für einen breiten öffentlichen Diskurs mit staatlicher Unterstützung ein – mit dem Ziel, einen klaren Regulierungsrahmen für Algorithmen zu erarbeiten. Insgesamt sind die Möglichkeiten der Bürgerbeteiligung an der politischen Entscheidungsfindung, z. B. unter Nutzung digitaler Technologien, auszubauen.


<hr class="hr_gradient"><div class="program_quote">
Polizei und Geheimdienste nehmen
aus Perspektive der Demokratiesicherung
eine ambivalente Rolle ein.
</div><hr class="hr_gradient">



**Polizei und Geheimdienste** nehmen aus Perspektive der Demokratiesicherung eine ambivalente Rolle ein. Einerseits ist es ihre unverzichtbare Kernaufgabe, die Verfassung und die Gesetze zu schützen, andererseits sind sie auf Grund des staatlichen Gewaltmonopols strukturell anfällig für Machtmissbrauch. Deshalb fordern wir zum einen eine angemessene personelle und technische Ausstattung, um Straftaten durch Präsenz verhindern oder schnell aufklären zu können. Zum anderen fordern wir auch eine unabhängige Kontrolle und wirksame Sanktionsmöglichkeiten bei Fehlverhalten. Eine Erkennungsnummer an der Uniform sollte bei allen Einsätzen getragen werden. Verdeckte Operationen müssen durch unabhängige Verfahren streng reguliert und nach einer angemessenen Frist bzw. nach richterlicher Anordnung veröffentlicht werden. Verdachtsunabhängige staatliche Massenüberwachung, z. B. mit Hilfe von Gesichtserkennung und Bewegungsprofilen, lehnen wir ab. Durch unabhängige Kontrollinstanzen ist sicherzustellen, dass die Grundrechte der Bürger\*innen gegenüber den staatlichen Organen gewahrt bleiben.

Wir betrachten den europäischen Einigungsprozess als große Errungenschaft. Allerdings weist die **Europäische Union** in ihrer aktuellen Form eine Reihe von Defiziten auf. Bisher orientiert sie sich einseitig an Wirtschaftsinteressen, was zu Recht zu Unmut und Akzeptanzproblemen in der Bevölkerung führt. Der Einfluss von Lobbyist\*innen und insbesondere kommerziellen Interessenvertreter\*innen ist deshalb vollständig transparent zu gestalten und sollte insgesamt eingeschränkt werden. Zusätzlich setzen wir uns für eine Wiederbelebung des EU-Verfassungsprozesses ein – mit dem Schwerpunkt auf bessere demokratische Legitimation der Institutionen und Gesetzgebungsverfahren. Erst wenn dieser Prozess abgeschlossen ist, können weitere nationalstaatliche Kompetenzen an die EU abgegeben werden.

Um unverhältnismäßig großen **Einfluss von wirtschaftlichen Interessen** auf politische Entscheidungen zu unterbinden, fordern wir einerseits deutlich mehr Transparenz und Kontrolle bei den Themen Parteienfinanzierung, Nebentätigkeiten und Lobbyismus. Andererseits sollte langfristig die Größe von Konzernen (und damit ihre Macht) durch ein strikteres Kartellrecht und eine stärkere Besteuerung begrenzt werden. Zudem setzen wir uns für internationale Regeln gegen ruinösen Deregulierungswettbewerb der Staaten untereinander ein.



### 3. Soziale Ungleichheit in Deutschland und Europa


Einkommen und Vermögen sind in Deutschland äußerst ungleich verteilt. Der Spitzensteuersatz ist seit 1953 von 95% auf 45% gesunken, während die Mehrwertsteuer seit 1968 von 10% auf 19% gestiegen ist. Wir fordern, dass die unteren Einkommensgruppen finanziell entlastet werden und der Reichtum der oberen Einkommensgruppen stärker besteuert wird. Für diese Umverteilung brauchen wir eine weitreichende **Reform des Steuersystems**. Insbesondere die Einkommenssteuer sollte nicht nur von der Höhe des Einkommens abhängig sein, sondern auch von der gesellschaftlichen Relevanz des jeweiligen Berufs. Wir brauchen eine offene Debatte darüber, welche Berufe für unsere Gesellschaft besonders wichtig sind und besser entlohnt werden müssen. Dementsprechend könnten z. B. Kranken- und Altenpfleger\*innen, Reinigungskräfte, Feuerwehrmänner/-frauen, Arbeitskräfte in landwirtschaftlichen Betrieben, Lehrer\*innen oder Erzieher\*innen steuerlich entlastet werden. Zusätzlich fordern wir eine Finanztransaktionssteuer, um den Finanzsektor stärker an der Finanzierung des Gemeinwesens zu beteiligen und schädliche Spekulationen einzudämmen.


<hr class="hr_gradient"><div class="program_quote">
Sorgearbeit, die meist von Frauen
geleistet wird, muss in Deutschland
mehr Anerkennung erfahren.
</div><hr class="hr_gradient">



Neben der bezahlten Erwerbsarbeit ist auch die sogenannte **Sorgearbeit** (Erziehung von Kindern, Pflege von Angehörigen) von enormer gesellschaftlicher Bedeutung. Diese Arbeit, die meist von Frauen geleistet wird, muss in Deutschland mehr Anerkennung erfahren. Denjenigen, die Sorgearbeit leisten, dürfen keine finanziellen Nachteile entstehen. Wir fordern ein staatlich gezahltes, unbürokratisch zu beantragendes Mindesteinkommen für alle, die wegen Pflege oder Kindererziehung keinem regulären Job nachgehen können. Diejenigen, die nur noch in Teilzeit arbeiten können, sollen über eine geringere Einkommenssteuer entlastet werden. Außerdem soll die komplette Zeit der geleisteten Sorgearbeit spürbar auf die Rente angerechnet werden.

Zur **Entlastung der unteren und mittleren Einkommensgruppen** fordern wir eine Ausweitung des sozialen Wohnungsbaus, niedrigere Preise für ÖPNV und Kinderbetreuung sowie eine stärkere Kontrolle von Mindestlohn und Mietpreisbremse. Bei Nicht-Einhaltung der gesetzlichen Vorgaben müssen hohe Strafen gezahlt werden. Auch das System der Sozialversicherungen muss neu organisiert werden. Insbesondere die Beitragsbemessungsgrenzen bei der gesetzlichen Renten-, Arbeitslosen-, Kranken- und Pflegeversicherung müssen sofort abgeschafft werden, da sie die oberen Einkommensgruppen einseitig bevorzugen. Durch eine entsprechende Umverteilung können untere und mittlere Einkommensgruppen entlastet werden.


<hr class="hr_gradient"><div class="program_quote">
Jeder Mensch hat das Recht auf
ein Mindesteinkommen  unabhängig
von Alter, Behinderung,
Krankheit oder Erwerbsarbeit.
</div><hr class="hr_gradient">



Jeder Mensch hat das Recht auf ein Mindesteinkommen – unabhängig von Alter, Behinderung, Krankheit oder Erwerbsarbeit. Das Zahlen einer **Grundsicherung** ist für uns eine gesellschaftliche Selbstverständlichkeit. Im derzeitigen System müssen Arbeitslose, Rentner\*innen und Erwerbsgeminderte jedoch immer wieder als Bittsteller\*innen auftreten und staatliche Leistungen hart erkämpfen. Diesen alltäglichen Erniedrigungen wollen wir ein Ende setzen. Alle Leistungen der Grundsicherung (Sozialgeld, Arbeitslosengeld II, Hilfe zum Lebensunterhalt, Grundsicherung im Alter und bei Erwerbsminderung) müssen unbürokratisch und zügig ausgezahlt werden. Sozialamt und Jobcenter müssen kundenfreundlich arbeiten. Der interne Sparzwang muss beendet werden.

Lebensqualität bedeutet auch, Zeit zu haben – Zeit für Familie, Freund\*innen, Gemeinschaft, zivilgesellschaftliches Engagement oder Freizeitaktivitäten. Menschen brauchen Zeit um zu überlegen, welche Träume und Ziele sie haben und welches Leben sie führen wollen. Vor allem braucht es Zeit, um zur Ruhe zu kommen, um durchzuatmen und uns nicht kaputt zu arbeiten. Besonders in den unteren Gehaltsgruppen fehlt es oft an dieser Zeit. Reinigungskräfte, Kassierer\*innen und Küchenhilfen können es sich nicht leisten, in Teilzeit zu arbeiten – schon gar nicht, wenn sie eine Familie zu versorgen haben. Die mittleren und oberen Gehaltsklassen könnten sich zwar für weniger Erwerbsarbeit entscheiden, spüren aber oft den sozialen Druck zur Vollzeitbeschäftigung, wollen ihre Karriere nicht aufs Spiel setzen oder überschätzen ihre persönliche Leistungsfähigkeit. Die steigenden Burnout-Raten sind Ausdruck dieser Entwicklung. Wir glauben, dass alle Menschen ein Recht auf freie Zeit haben. Das Leben muss mehr als Erwerbsarbeit sein. Deshalb fordern wir die schrittweise **Einführung der 30-Stunden-Woche** in allen Berufsgruppen. Der technische Fortschritt ermöglicht uns eine deutliche Reduktion der Arbeitsbelastung. Es liegt an uns, diese Chance im Sinne der Lebensqualität zu nutzen.


<hr class="hr_gradient"><div class="program_quote">
Wir brauchen Zeit, um zur Ruhe
zu kommen, um durchzuatmen
und uns nicht kaputt zu arbeiten.
</div><hr class="hr_gradient">



Auch das deutsche Bildungssystem muss dringend reformiert werden. Weder die Ergebnisse der PISA-Studien noch die internationalen Rankings zur Chancengleichheit im Bildungssystem sind zufriedenstellend. Um auch sozial Schwächeren einen gesellschaftlichen Aufstieg zu ermöglichen, setzen wir uns deshalb für **integrative und inklusive Ganztagsschulen** und ein **längeres gemeinsames Lernen** ein. Die Trennung der Schüler\*innen nach der 4. Klasse sowie die Aufteilung in Regel- und Förderschulen sind für uns unbefriedigende Zustände, die es zu überwinden gilt. Wir fordern eine 9-jährige verpflichtende Primärschule für alle, die Ablösung von Förderschulen durch die Einrichtung spezieller Förderklassen an regulären Schulen sowie eine bessere Ausstattung der Schulen mit entsprechendem Hilfspersonal. Insbesondere in den Bereichen Sozialarbeit, Sprachförderung und Sonderpädagogik braucht das reguläre Lehrpersonal erheblich mehr Unterstützung. Zudem muss die Aus- und Weiterbildung des regulären Lehrpersonals im Bereich Pädagogik verbessert werden. Die Schule darf kein Ort der reinen Faktenvermittlung sein, sondern muss Kinder zum eigenständigen reflektierten Denken motivieren und befähigen. Hier geht es auch um die Vermittlung vielfältiger sozialer Kompetenzen sowie die **Bildung im Bereich Nachhaltigkeit und Antidiskriminierung**. Inklusion (Einbeziehung von Kindern mit Behinderung) und Integration (Einbeziehung von Kindern mit Migrationshintergrund) sollen durch die Einrichtung spezieller Förderklassen an regulären Schulen sowie die Schaffung eines Kurssystems gewährleistet werden. In den meisten Fächern sollen Kurse mit verschiedenen Schwierigkeitsstufen angeboten werden, die den Schüler\*innen je nach Leistungsniveau individuell zugewiesen werden.



<hr class="hr_gradient"><div class="program_quote">
Die Schule darf kein Ort der reinen
Faktenvermittlung sein, sondern muss
Kinder zum eigenständigen reflektierten
Denken motivieren und befähigen.
</div><hr class="hr_gradient">




Um Eltern und Kinder bestmöglich zu fördern, setzen wir uns für ein **flächendeckendes Beratungsangebot für Familien** zur Vermittlung von psychologischen und pädagogischen Kompetenzen und Unterstützungsangeboten ein. Für diese Beratungen sollen externe Sozialarbeiter\*innen an den Schulen als Ansprechpartner\*innen zur Verfügung stehen. Um Kinderarmut vorzubeugen, fordern wir eine Bevorzugung von Alleinerziehenden bei der Vergabe von Kinderbetreuungsplätzen sowie eine steuerliche **Entlastung Alleinerziehender** über Senkung der Einkommenssteuer im unteren Lohnbereich. Außerdem fordern wir die Abschaffung des Ehegattensplittings, um eine Kindergrundsicherung von 500 € pro Kind und Monat finanzieren zu können. Die 500 € **Kindergrundsicherung** werden auf eventuelle Sozialleistungen nicht angerechnet. Neben finanzieller Unterstützung braucht es auch mehr kostenlose Freizeit- und Vereinsangebote für Familien. Insbesondere Sportvereine sollten stärker staatlich bezuschusst werden. Mit diesen Maßnahmen möchten wir erreichen, dass Deutschland für Familien wieder attraktiv wird.



<hr class="hr_gradient"><div class="program_quote">
Alle in Deutschland lebenden
Menschen brauchen eine angemessene
medizinische Versorgung.
</div><hr class="hr_gradient">


Die derzeitigen Freiwilligendienste (Freiwilliges Soziales Jahr, Freiwilliges Ökologisches Jahr, Bundesfreiwilligendienst) sollen im **Allgemeinen Gesellschaftsdienst** (AGD) zusammengelegt werden. Freiwillige, die sich für den AGD entscheiden, leisten 10 Monate Unterstützungsarbeit in gesellschaftlich relevanten Bereichen und bekommen anschließend zwei Monate bezahlten Urlaub. AGD-Freiwillige werden bei der Vergabe von Studien- und Ausbildungsplätzen bevorzugt. Außerdem wird das AGD-Jahr doppelt auf die Rente angerechnet. Die Kosten für den AGD werden gemeinsam von Bund und Arbeitgebern getragen.

Im Bereich Gesundheit fordern wir eine **solidarische Bürgerversicherung** für alle. Private und gesetzliche Krankenversicherung sollen in einem gemeinsamen Modell vereinigt werden. Für die Beiträge zur solidarischen Bürgerversicherung werden sämtliche Einkunftsarten (Löhne, Gehälter, Zinsen, Dividenden, Tantiemen, Miet- und Pachterlöse) herangezogen. Um allen in Deutschland lebenden Menschen eine angemessene medizinische Versorgung zur Verfügung zu stellen, wird der Leistungskatalog im Vergleich zur bisherigen gesetzlichen Krankenversicherung erweitert. Insbesondere präventive Gesundheitsleistungen (speziell für gefährdete Berufsgruppen) und alternative Heilmethoden sollen stärker gefördert werden.




<hr class="hr_gradient"><div class="program_quote">
Die gesellschaftliche Tendenz zur
Privatisierung und Ökonomisierung hat
insbesondere im Gesundheitssystem
massive negative Auswirkungen.
</div><hr class="hr_gradient">


Die in vielen gesellschaftlichen Bereichen stattfindende **Tendenz zur Privatisierung und Ökonomisierung** hat insbesondere im Gesundheitssystem massive negative Auswirkungen. Vor allem die als Aktiengesellschaften organisierten Klinikgruppen arbeiten in der Regel gewinnorientiert und vernachlässigen die Bedürfnisse der Patient\*innen. Auch freigemeinnützige Träger (Kirchen, Wohlfahrtsverbände) arbeiten zunehmend gewinnorientiert. Wir fordern, dass alle privaten Krankenhäuser wieder in eine öffentliche Trägerschaft zurückgeführt werden. Von allen freigemeinnützigen Trägern verlangen wir einen öffentlich zugänglichen Geschäftsbericht sowie die Achtung von Tarifverträgen und grundlegenden Arbeitnehmerrechten.

Die derzeitigen **Abrechnungssysteme im stationären und ambulanten Bereich** (DRG-System, Budgetierung) führen nicht nur zu einer hohen Belastung des medizinischen Personals mit bürokratischen Aufgaben, sondern begünstigen auch medizinische Fehlbehandlungen (Über- oder Unterversorgung). Wir fordern feste Gehälter für alle niedergelassenen Ärzt\*innen und alle Angestellten in öffentlichen Krankenhäusern sowie mehr Personal in Krankenhäusern durch feste Personal-Patienten-Schlüssel. Zusätzlich sollen alle notwendigen medizinischen Leistungen (ohne Budgetgrenzen) von der Krankenkasse bezahlt werden. Dies verhindert, dass sich einzelne Ärzt\*innen oder Klinikbetreiber finanziell bereichern oder an Leistungen gespart wird, die medizinisch notwendig sind. Um unnötige Operationen zu vermeiden, sollen für alle nicht-akuten invasiven Eingriffe unabhängige Zweitmeinungen eingeholt werden.

Um die **Pflegeberufe** attraktiver zu machen, braucht es auch in den Alten- und Pflegeheimen einen festen Personalschlüssel. Zusätzlich sollen alle Pflegekräfte durch die Einführung der 30-Stunden-Woche und den verstärkten Einsatz von nicht-professionellem Unterstützungspersonal entlastet werden. Das Hilfspersonal soll über den AGD gestellt werden. Um möglichst vielen Menschen ein würdiges Altern zu ermöglichen, müssen **alternative Wohn- und Pflegekonzepte** (wie z. B. Seniorensiedlungen, Mehrgenerationenhäuser oder das niederländische Buurtzorg-Modell) erprobt werden.

<hr class="hr_gradient"><div class="program_quote">
In den letzten Jahren hat sich die
Gesellschaft bezüglich Ungleichheitsfragen
stärker polarisiert. Wir möchten diesen 
Spaltungstendenzen entgegenwirken.
</div><hr class="hr_gradient">

Auch beim Thema **Migration** besteht großer Handlungsbedarf. Dabei ist die sogenannte "irreguläre Migration" sowohl für die betroffenen Menschen als auch für die beteiligten Länder hochproblematisch. Aus grundsätzlicher humanitärer Verantwortung und in Anerkennung der Verantwortung für den Kolonialismus müssen Deutschland und Europa wesentlich mehr tun, um Menschen in Not zu helfen. Wir fordern konkret: 1. Der Etat des UNHCR muss deutlich erhöht werden, um schnelle Hilfe in betroffenen Regionen leisten zu können. 2. Ein Asylantrag soll in Zukunft sowohl in allen deutschen Botschaften als auch in den Flüchtlingslagern des UNHCR möglich sein. Potenzielle Antragsteller*innen sollen dabei realistisch über Risiken und Chancen einer Migration nach Deutschland aufgeklärt werden. Zusätzlich wollen wir 3. das deutsche Kontingent der aufzunehmenden Geflüchteten beim UNHCR deutlich erhöhen. Damit soll auch die Planbarkeit der Geflüchtetenzahlen verbessert werden und so kann der Integrationsprozess sofort beginnen. Neben politisch Verfolgten und Opfern gewaltsamer Konflikte sollen auch Betroffene schwerer humanitärer Katastrophen ein Recht auf Asyl bekommen, falls sie in der Region nicht ausreichend versorgt werden können.

Perspektivlosigkeit, Traumatisierung und teilweise problematische Sozialisationen verursachen Konflikte und Probleme, die sich durch Ignoranz und Desinteresse noch ausweiten können. Wir fordern eine strukturell verankterte Willkommenskultur und eine **aktive Integrationspolitik**, die es Zugewanderten ermöglicht, selbstbestimmt an der Gesellschaft teilzuhaben und sie mitzugestalten. Missverständnissen, Unsicherheit und eventuellen Konflikten muss durch Aufklärung, Hilfsangebote und klare Perspektiven proaktiv entgegengetreten werden. Wir setzen uns dafür ein, dass die Mehrheitsgesellschaft weltoffen und tolerant gegenüber Menschen anderer Herkunft ist. Bildungsangebote im Bereich Antidiskriminierung sind dafür eine wichtige Grundlage. Selbstverständlich fordern wir von allen Menschen, die in Deutschland leben, dass sie sich an die Gesetze halten, die Rechte und Bedürfnisse ihrer Mitmenschen respektieren sowie die freiheitlich-demokratische Grundordnung anerkennen.

<hr class="hr_gradient"><div class="program_quote">
Es gibt keinen absoluten Mangel an
öffentlichen Gütern – wir haben es
nur mit einem Verteilungsproblem zu tun.
</div><hr class="hr_gradient">


Um **Konkurrenzsituationen** wegen knapper Güter (Wohnungen, Plätze in Kitas und Schulen, Arzttermine) zu vermeiden, brauchen wir erhebliche Investitionen in Bildung, Kinderbetreuung, Wohnraum und das Gesundheitssystem. Wir finden, dass Arme nicht gegen Arme ausgespielt werden sollten. Es gibt keinen absoluten Mangel an öffentlichen Gütern – wir haben es nur mit einem Verteilungsproblem zu tun.

Auch auf europäischer Ebene brauchen wir mehr Solidarität. Die **Wohlstandsgefälle innerhalb der Europäischen Union** sind gravierend, die derzeitigen Mechanismen der Umverteilung unzureichend. Wir müssen dafür sorgen, dass alle Menschen innerhalb der EU ein menschenwürdiges Leben führen können. Das heißt: Ausbau der sozialen Sicherungssysteme, Zugang zu grundlegender medizinischer Versorgung und Bildung, Zugang zu bezahlbarem Wohnraum sowie Schaffung hochwertiger Arbeitsplätze. Die EU darf nicht nur Hüterin von Wettbewerb und offenen Märkten sein, sie muss auch für soziale Gerechtigkeit in ihren Mitgliedsländern sorgen. Erst wenn alle Bürger\*innen der EU eine angemessene Rente, ein angemessenes Arbeitslosengeld, würdige Arbeitsbedingungen sowie ausreichenden Zugang zu Bildung, Gesundheit und Wohnraum haben, wird die EU von der breiten Bevölkerung akzeptiert werden. Solange sie Großkonzerne schützt, Steueroasen toleriert, marode Banken rettet und einzelnen Ländern unsoziale Sparprogramme auferlegt, wird sie keine politische Zukunft haben.





### 4. Globale Ungerechtigkeiten

Bewaffnete Konflikte sind auch heute noch in vielen Teilen der Welt bittere Realität. Ohne Frieden und Sicherheit ist es unmöglich, einen demokratischen Staat und ein funktionierendes Wirtschaftssystem aufzubauen. Die EU muss deshalb ihren Schwerpunkt auf die **Verhinderung und Beilegung bewaffneter Konflikte** legen. Sie muss außerdem sorgfältig analysieren, inwiefern ihre eigene Handels-, Außen- und Sicherheitspolitik zur Verfestigung von Gewalt in anderen Ländern führt. Neben Diplomatie und dem sofortigen Stopp von Waffenlieferungen in Krisengebiete werden auch faire Handelsabkommen und die Unterstützung der lokalen Wirtschaft vonnöten sein. Die besten Friedensabkommen sind brüchig, solange die vor Ort lebenden Menschen keine ökonomische Zukunftsperspektive haben.

Auch unabhängig von bewaffneten Konflikten brauchen wir **faire Handelsabkommen** zwischen Industrie-, Schwellen- und Entwicklungsländern. Wir können nicht länger dabei zusehen und daran mitwirken, wie für unseren Wohlstand andere Länder ausgebeutet werden. Viele Produkte, die innerhalb der EU erhältlich sind, können nur deshalb so billig sein, weil die Herkunftsländern wirtschaftlich von den Industrienationen abhängig sind. Dies hat massive Auswirkungen auf das Leben der lokalen Bevölkerung: Kinder gehen nicht zur Schule, weil sie arbeiten müssen. Eltern verdienen einen Hungerlohn, mit dem sie kaum ihre Familie ernähren können. Multinationale Unternehmen kaufen in großem Stil landwirtschaftliche Flächen auf und rauben den lokalen Bauern ihre Lebensgrundlage. Boden, Wasser und Luft werden durch unzureichende Umweltauflagen verschmutzt und fehlender Arbeitsschutz gefährdet die Gesundheit der lokalen Arbeiter\*innen. Wir fordern **verbindliche Umwelt- und Fair-Trade-Standards** für alle innerhalb der EU erhältlichen Produkte. Die Importeure haben dabei die Nachweispflicht. Sie müssen zeigen, dass die von ihnen eingeführten Güter unter sozialen und ökologischen Bedingungen produziert wurden. Auch die Spekulation mit Boden und Nahrungsmitteln muss ein Ende haben. Die **Finanztransaktionssteuer** kann dazu einen entscheidenden Beitrag leisten.




<hr class="hr_gradient"><div class="program_quote">
Wir können nicht länger dabei
zusehen und daran mitwirken, wie
für unseren Wohlstand andere
Länder ausgebeutet werden.
</div><hr class="hr_gradient">



Um den sozialen und ökologischen Wandel weltweit zu fördern, setzen wir uns für einen **globalen Informationsaustausch** zwischen Unternehmen, zivilgesellschaftlichen Organisationen und politischen Institutionen ein. Dieser sollte durch entsprechende EU-Programme unterstützt werden. Austausch und gegenseitige Beratung sollen dabei helfen, die Fehler, die andere gemacht haben, nicht selbst zu wiederholen. Gelungene Projekte können im eigenen Land wiederholt oder angepasst werden. Ganz gleich, ob es sich um die Entwicklung eines Elektromotors, eines Wahlsystems oder eines Friedensabkommens handelt: Gegenseitige Unterstützung ist wichtig und verhindert oftmals große Fehlentwicklungen. Wir dürfen uns hier nicht länger voneinander abschotten. Das bedeutet auch, dass wir in Zukunft viel offener mit Wissen umgehen müssen. Betriebs- und Geschäftsgeheimnisse sowie das Patentsystem müssen vor diesem Hintergrund kritisch hinterfragt werden.

Gelingt es uns, die Folgen des Klimawandels zu minimieren, einen fairen Handel zu etablieren, die Macht multinationaler Unternehmen einzuschränken sowie gewaltsame Konflikte beizulegen, brauchen wir keine klassische **Entwicklungshilfe** mehr. Auch ein Großteil der aktuellen Fluchtbewegungen und das damit verbundene Leid könnte somit verhindert werden. Natürlich sollte die EU weiterhin lokale Organisationen unterstützen (z. B. in den Bereichen Friedensarbeit, Armutsbekämpfung, Bildung, soziale Gerechtigkeit, Klima und Umwelt). Sie sollte aber keineswegs von oben herab über Hilfsprogramme entscheiden, die an den strukturellen Problemen vorbeigehen und ihre eigene Machtposition unhinterfragt lassen. Auch hier ist ein Dialog auf Augenhöhe nötig.



### Fazit


Die Menschheit sieht sich im 21. Jahrhundert sowohl immensen Problemen als auch enormen Chancen gegenüber. Die derzeitigen wirtschaftlichen und politischen Konzepte sind Relikte der Vergangenheit, die die Probleme zum großen Teil verursacht haben. Eine Lösung ist von ihnen nicht zu erwarten. Das gegenwärtige Wirtschaftsmodell, das auf beständiges Anwachsen von Konsum und Produktion abzielt und dabei unvermeidlich Menschen und Natur schadet, kann nicht dauerhaft bestehen. In den aktuellen politischen Entscheidungsstrukturen wird das zerstörerische Potenzial des ungezügelten Kapitalismus schlicht ignoriert. Dies halten wir für rücksichtslos und unverantwortlich. Eine realistische Utopie, d. h. ein wirklich zukunftsfähiges politisches Gesamtkonzept, fehlt bislang.


<hr class="hr_gradient"><div class="program_quote">
Das gegenwärtige Wirtschaftsmodell,
das auf beständiges Anwachsen
von Konsum und Produktion abzielt,
kann nicht dauerhaft bestehen.
</div><hr class="hr_gradient">



Wenn die Errungenschaften der Zivilisation erhalten und sogar ausgebaut werden sollen, ist eine **sozial-ökologische Transformation der Gesellschaft** unumgänglich. Die Vorschläge und Forderungen in diesem Programm dienen dazu, eine solche Transformation anzustoßen. Sie stellen einige vermeintliche Gewissheiten in Frage, etwa dass das Schaffen von Arbeitsplätzen per se ein politischer Erfolg ist. Zudem enthält das Programm durchaus unbequeme Punkte, wie z. B. eine spürbare Erhöhung der Energiepreise. Um es klar und deutlich zu sagen: Wir halten es sowohl für notwendig als auch erstrebenswert, dass weniger gekauft, geflogen und gearbeitet wird. Notwendig aus simpler Einsicht in die Naturgesetze und erstrebenswert aus der Erkenntnis heraus, dass Erwerbsarbeit und Konsum nicht der Sinn und Zweck eines erfüllten Lebens sind. Was zählt, ist die Lebensqualität.

Damit die Transformation nicht zu einem Ansteigen sondern zu einem Rückgang sozialer Ungerechtigkeit führt, enthält das Programm zahlreiche Ausgleichs- und Umverteilungsmaßnahmen. Anerkennung von Sorgearbeit, Preissenkung des ÖPNV und eine Verlagerung der Steuerlast von Erwerbsarbeit zu Ressourcenverbrauch, Kapitaleinkommen und Vermögen sind dafür erste Schritte.

Die große Herausforderung für die Demokratie besteht darin, für diejenigen Maßnahmen Mehrheiten hervorzubringen, die zwar unpopulär, aber für eine positive Entwicklung notwendig sind. **Lebensqualität** ist dabei der Maßstab, an dem sich alle politischen Entscheidungen auszurichten haben.




## Schlussbemerkung (=Vorbemerkung)

Dies ist die erste Version unseres Parteiprogramms. Es soll kontinuierlich weiterentwickelt und kritisch hinterfragt werden. Wir bitten deshalb aktiv um **Feedback**. Welche Probleme haben wir übersehen und welche überbewertet? Welche Vorschläge sind zu zaghaft, welche zu weitgehend? Wir freuen uns über Feedback und ehrliche Meinung. Als Gründungsinitiative einer neuen Partei bieten wir aktiven Gestaltungsspielraum. Lasst uns diesen zum Wohle der Menschen nutzen!
