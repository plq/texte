# PLQ Grundwerte

## Lebensqualität heißt: Gutes Leben für alle

Unter "Lebensqualität" verstehen wir die Möglichkeit, ein gutes, erfülltes Leben zu führen – und zwar für alle Menschen, auch in anderen Teilen der Welt und einschließlich zukünftiger Generationen. Die Basis dafür ist die Erfüllung von Grundbedürfnissen wie Ernährung, Gesundheit, Wohnen, Sicherheit und soziale Teilhabe. Unser übergeordnetes politisches Ziel ist es, die gesellschaftlichen und materiellen Grundvoraussetzungen für Lebensqualität zu schaffen, zu fördern und zu erhalten.
Aus diesem Leitbild ergeben sich folgende Grundwerte:

## Menschenrechte

Wir setzen uns für die konsequente Einhaltung und Durchsetzung der universellen Menschenrechte ein.

Wir setzen uns dafür ein, dass jeder Mensch in Würde und Sicherheit leben und seine Persönlichkeit nach eigenen Fähigkeiten und Bedürfnissen selbstbestimmt entfalten kann. Die Freiheit des Individuums ist ein hohes und schützenswertes Gut. Sie findet ihre Grenzen nur dort, wo die Sicherheit, die Würde oder die Freiheitsrechte anderer Menschen eingeschränkt oder die Umwelt gefährdet werden.

Wir setzen uns dafür ein, dass alle Menschen die gleichen Rechte und Chancen auf ein gutes Leben haben. Individuelle Benachteiligung und Herabwürdigung sowie gruppenbezogene Menschenfeindlichkeit auf Grund von (zugeschriebenen) Merkmalen wie Geschlecht, Hautfarbe, Nationalität, ethnischer oder sozialer Herkunft, Weltanschauung, sexueller Orientierung, körperlicher oder geistiger Gestalt lehnen wir ab.

Wir setzen uns für Konfliktprävention, Deeskalation und friedliche Konfliktlösungen ein.

## Solidarität

Wir streben eine solidarische Gesellschaft an, in der gegenseitige (Mit-)Verantwortung, Rücksichtnahme und Unterstützung aller Menschen der Normalzustand ist. Pflichten sind nach den Möglichkeiten des einzelnen Menschen auszurichten.

Rücksichtslosen Egoismus und die Durchsetzung von Ansprüchen zu Lasten der legitimen Bedürfnisse Anderer lehnen wir ab.

Wir setzen uns dafür ein, dass alle Menschen – auch in anderen Teilen der Welt und auch die nachfolgenden Generationen – die Möglichkeit zu einem guten Leben haben.

Wir setzen uns für eine angemessene Umverteilung materieller Güter ein. Insbesondere große Kapitalgesellschaften müssen ihren gerechten Beitrag zum Allgemeinwohl leisten.

## Demokratie und Antitotalitarismus

Wir setzen uns für die Stärkung demokratischer Prinzipien ein, wie Gewaltenteilung, Versammlungs-, Presse- und Meinungsfreiheit, und streben ihre Fortentwicklung im Sinne von breiter Partizipation und Zustimmungsfähigkeit an.

Wir setzen uns für eine konsequente Trennung von Staat und religiösem Bekenntnis, sowie für Glaubensfreiheit ein. Jede Religionsgemeinschaft muss die Freiheitsrechte der Menschen anerkennen. Die Unantastbarkeit der Menschenwürde steht im Zweifelsfalle über allen religiösen Normen.

Wir lehnen alle Bestrebungen ab, ein totalitäres Staatssystem zu etablieren oder darauf hinzuwirken. Wir wenden uns insbesondere gegen jedwede Bestrebung, Faschismus oder Nationalsozialismus zu bewerben oder gar umzusetzen.

Wir setzen uns für informationelle Selbstbestimmung ein. Personenbezogene Daten müssen vor dem unberechtigten Zugriff privater oder staatlicher Akteur*innen geschützt sein.

## Wirtschaft

Unsere Wirtschaftspolitik richtet sich nicht auf Wachstum des Bruttoinlandsprodukts, sondern darauf, allen Menschen einschließlich zukünftigen Generationen ein gutes Auskommen zu ermöglichen. Die Wirtschaft ist für die Menschen da, nicht die Menschen für die Wirtschaft. Wirtschaftlicher Wettbewerb hat sich der Humanität unterzuordnen.

Wir setzen uns für ein vorausschauendes, gerechtes Wirtschaftssystem ein, das die begrenzte Tragfähigkeit unseres Planeten respektiert, fairen Handel und faire Arbeitsbedingungen fördert, und für wirtschaftlich Schwächere mit aufkommt.

Wir sehen im Markt ein Instrument, um Warenaustausch und individuelle wirtschaftliche Tätigkeit zu kanalisieren. Allerdings sind Marktmechanismen allein nicht in der Lage, die Gesamtwirtschaft im Sinne der Nachhaltigkeit und Gerechtigkeit zu steuern. Dies ist Aufgabe des demokratischen Staates.

## Umwelt, Biodiversität und Klima

Wir sehen eine gesunde Natur – stabiles Klima und intakte Ökosysteme, saubere Umwelt und Biodiversität – als Grundlage für menschliches Leben. Natur ist schützenswert, auch dann, wenn kein wirtschaftlicher Nutzen aus ihr gezogen werden kann.

Wir fordern und fördern den Übergang zu einer nachhaltigen Lebensweise, von der globalen bis zur individuellen Ebene. Dies bedeutet unter anderem: zügige Reduzierung von Treibhausgasen, verantwortungsvoller Umgang mit Rohstoffen und Land, sowie Erhalt und Wiederherstellung von Ökosystemen.

Den Übergang zu einer nachhaltigen Gesellschaft wollen wir sozial gerecht gestalten.

## Internationales

Wir bekennen uns zur internationalen Solidarität und Zusammenarbeit, insbesondere zur Bewältigung der großen globalen Herausforderungen wie bewaffnete Konflikte und Klimawandel.

Wir bekennen uns zur Präambel sowie den Zielen und Grundsätzen der Charta der Vereinten Nationen. Wir halten eine Demokratisierung ihrer Struktur und eine gerechte Repräsentation aller Menschen in ihren Entscheidungsgremien für dringend erforderlich.

Wir setzen uns für eine Welt ein, in der die Beschränkung der Freizügigkeit durch nationale Grenzen überflüssig ist. Die Schaffung gleichwertiger Lebensverhältnisse aller Menschen ist unser Ziel.

Wir sehen ein vereintes Europa als den Garanten für Frieden und allgemeinen Wohlstand für die Menschen in Europa. Wir setzen uns für eine demokratische Weiterentwicklung der EU ein.

## Nachvollziehbare Politik

Wir setzen uns dafür ein, dass politische Entscheidungen auf Basis des Standes der Wissenschaft getroffen und bei wesentlich verbessertem Kenntnisstand auch revidiert werden. Politische Entscheidungen sind bezüglich Zweck, Maßnahmen und getroffenen Abwägungen zu begründen. Relevante Einwände und Bedenken sind nachvollziehbar zu dokumentieren.

Wir setzen uns für eine starke Regulierung von Lobbyismus ein. Das Einbringen von externem Sachverstand in den politischen Prozess muss transparent erfolgen und primär dem Allgemeinwohl dienen. Privatwirtschaftliche Interessen dürfen nicht stärker repräsentiert werden als zivilgesellschaftliche.

Wir setzen uns für eine möglichst weitgehende Transparenz der öffentlichen Verwaltung ein. Dies betrifft sowohl im Verwaltungshandeln entstehende Daten ([OpenData](https://de.wikipedia.org/wiki/Open_Data)) als auch Rechtstexte wie Gesetze, Verordnungen und Verträge.

Wir sehen in der Zuverlässigkeit und Manipulationssicherheit von Informationen eine notwendige Voraussetzung für die Selbstbestimmung und die politische Mündigkeit. Wir setzen uns für die Sicherheit von Informations- und Kommunikationssystemen und für eine weitreichende Überprüfbarkeit und Authentifizierbarkeit von öffentlich relevanten Informationen ein.

## Beteiligungs- und Kommunikationsregeln

Diese Grundwerte spiegeln unsere normativen Überzeugungen wider. Sie begründen daher sowohl unsere konkreten politischen Ziele als auch die Art und Weise, wie wir Politik betreiben. Daraus resultieren die folgenden Beteiligungs- und Kommunikationsregeln:

Wir streben einen konstruktiven und respektvollen Dialog an, sowohl parteiintern als auch nach außen. Dieser Anspruch gilt insbesondere auch für die Kommunikation bezogen auf und gerichtet an den sogenannten politischen Gegner.

Im Falle von unterschiedlichen Auffassungen streben wir inhaltsbezogene Diskussionen auf Basis von gut begründeten Argumenten und – falls erforderlich – mit Verweisen auf wissenschaftliche Publikationen an. Faktenbasiertes kritisches Hinterfragen vermeintlicher Gewissheiten ist für uns Teil eines produktiven Erkenntnisprozesses. Hingegen sehen wir in spekulativem und destruktivem Anzweifeln des Standes der Wissenschaft ohne überprüfbare Argumente eine Behinderung eines konstruktiven Diskurses. Solches Verhalten lehnen wir ab.

Im Falle eines innerparteilichen Konfliktes erwarten wir von allen Beteiligten ein konstruktives Mitwirken an dessen zügiger Beilegung, z. B. in Form einer kooperativen Mediation.

Im Widerspruch zu einer Mitgliedschaft in der PLQ stehen:

- Äußerungen oder Verhalten zum mutwilligen Schaden einzelner Mitglieder oder der Partei,
- gruppenbezogene Menschenfeindlichkeit und persönliche Beleidigungen oder Herabwürdigungen, insbesondere auf Grund von (zugeschriebenen) Merkmalen wie Geschlecht, Hautfarbe, Nationalität, ethnischer oder sozialer Herkunft, Weltanschauung, sexueller Orientierung, körperlicher oder geistiger Gestalt.

Parallele Mitgliedschaften in anderen politischen Parteien sind anzugeben, gemeinsam mit einer Erläuterung wie Interessenskonflikte zu vermeiden sind. Frühere Mitgliedschaften in anderen politischen Parteien sind ebenfalls anzugeben.
